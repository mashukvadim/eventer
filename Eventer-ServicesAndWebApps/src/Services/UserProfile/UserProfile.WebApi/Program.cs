﻿using System;
using System.Diagnostics;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;

namespace UserProfile.WebApi
{
    using Infrastructure;

    public class Program
    {
        public static int Main(string[] args)
        {
            try
            {
                var host = BuildWebHost(args);

                host.SeedContext<UserProfileContext>();

                host.Run();

                return 0;
            }
            catch (Exception ex)
            {
                //Log.Fatal(ex, "Program terminated unexpectedly ({ApplicationContext})!", AppName);

                // !!!HERE SHOULD BE LOGGER!!!
                Debug.Write(ex.Message);

                return 1;
            }
            finally
            {
                //Log.CloseAndFlush();
            }
        }

        private static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                   .CaptureStartupErrors(false)
                   .UseStartup<Startup>()
                   .Build();
    }
}
