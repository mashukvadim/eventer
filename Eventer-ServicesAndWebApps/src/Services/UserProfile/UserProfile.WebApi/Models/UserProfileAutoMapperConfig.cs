﻿using UserProfile.WebApi.Models.DatabaseModels;
using UserProfile.WebApi.Models.ViewModels;

namespace UserProfile.WebApi.Models
{
    public class UserProfileAutoMapperConfig : AutoMapper.Profile
    {
        public UserProfileAutoMapperConfig()
        {
            CreateMap<Profile, ProfileViewModel>();
            CreateMap<Address, AddressViewModel>();
            CreateMap<Location, LocationViewModel>();
            CreateMap<Profile, FullProfileViewModel>();
        }
    }
}
