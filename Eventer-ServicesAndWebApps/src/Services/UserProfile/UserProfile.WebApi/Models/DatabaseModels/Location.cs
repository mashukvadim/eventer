﻿using System;
using UserProfile.WebApi.Models.Interfaces;

namespace UserProfile.WebApi.Models.DatabaseModels
{
    public class Location : ILocation
    {
        public Guid Id { get; set; }

        public string Latitude { get; set; }

        public string Longitude { get; set; }

        public string TimeZone { get; set; }
    }
}
