﻿using System;
using UserProfile.WebApi.Models.Interfaces;

namespace UserProfile.WebApi.Models.DatabaseModels
{
    public class Profile : IProfile
    {
        public Guid Id { get; set; }

        public string AvatarLink { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string GenderType { get; set; }

        public string University { get; set; }

        public string Race { get; set; }

        public DateTime DateOfBirth { get; set; }

        public virtual Address Address { get; set; }

        public virtual Location Location { get; set; }
    }
}
