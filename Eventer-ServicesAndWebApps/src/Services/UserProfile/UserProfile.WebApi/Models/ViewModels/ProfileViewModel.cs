﻿using System;
using UserProfile.WebApi.Models.Interfaces;

namespace UserProfile.WebApi.Models.ViewModels
{
    public class ProfileViewModel : IProfile
    {
        public Guid Id { get; set; }

        public string AvatarLink { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string GenderType { get; set; }

        public string University { get; set; }

        public string Race { get; set; }

        public DateTime DateOfBirth { get; set; }
    }
}
