﻿using System;
using UserProfile.WebApi.Models.Interfaces;

namespace UserProfile.WebApi.Models.ViewModels
{
    public class LocationViewModel : ILocation
    {
        public Guid Id { get; set; }

        public string Latitude { get; set; }

        public string Longitude { get; set; }

        public string TimeZone { get; set; }
    }
}
