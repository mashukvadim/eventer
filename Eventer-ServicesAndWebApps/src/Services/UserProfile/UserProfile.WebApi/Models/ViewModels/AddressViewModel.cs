﻿using System;
using UserProfile.WebApi.Models.Interfaces;

namespace UserProfile.WebApi.Models.ViewModels
{
    public class AddressViewModel : IAddress
    {
        public Guid Id { get; set; }

        public string City { get; set; }

        public string Country { get; set; }

        public string CountryCode { get; set; }

        public string PostalCode { get; set; }

        public string State { get; set; }

        public string StateAbbr { get; set; }

        public string StreetName { get; set; }

        public uint StreetNumber { get; set; }

        public string StreetSuffix { get; set; }
    }
}
