﻿using System;

namespace UserProfile.WebApi.Models.Interfaces
{
    public interface IAddress
    {
        Guid Id { get; set; }

        string City { get; set; }

        string Country { get; set; }

        string CountryCode { get; set; }

        string PostalCode { get; set; }

        string State { get; set; }

        string StateAbbr { get; set; }

        string StreetName { get; set; }

        uint StreetNumber { get; set; }

        string StreetSuffix { get; set; }
    }
}
