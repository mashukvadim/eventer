﻿using System;

namespace UserProfile.WebApi.Models.Interfaces
{
    public interface ILocation
    {
        Guid Id { get; set; }

        string Latitude { get; set; }

        string Longitude { get; set; }

        string TimeZone { get; set; }
    }
}
