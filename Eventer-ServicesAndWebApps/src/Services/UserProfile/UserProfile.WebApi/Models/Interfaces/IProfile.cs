﻿using System;

namespace UserProfile.WebApi.Models.Interfaces
{
    public interface IProfile
    {
        Guid Id { get; set; }

        string AvatarLink { get; set; }

        string FirstName { get; set; }

        string LastName { get; set; }

        string GenderType { get; set; }

        string University { get; set; }

        string Race { get; set; }

        DateTime DateOfBirth { get; set; }
    }
}
