﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using UserProfile.WebApi.Models.ViewModels;
using UserProfile.WebApi.Services.Interfaces;

namespace UserProfile.WebApi.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class ProfileController : ControllerBase
    {
        private readonly IProfileService _profileService;
        private readonly IAddressService _addressService;
        private readonly ILocationService _locationService;

        private readonly IMapper _mapper;

        public ProfileController(IMapper mapper, IAddressService addressService, ILocationService locationService, IProfileService profileService)
        {
            _addressService = addressService ?? throw new ArgumentNullException(nameof(mapper));
            _locationService = locationService ?? throw new ArgumentNullException(nameof(mapper));
            _profileService = profileService ?? throw new ArgumentNullException(nameof(mapper));

            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<ProfileViewModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetProfiles()
        {
            var profiles = await _profileService.GetProfilesAsync();
            var profilesViewModel = _mapper.Map<IEnumerable<ProfileViewModel>>(profiles);

            return new JsonResult(profilesViewModel);
        }

        [HttpGet("{profileId}")]
        [ProducesResponseType(typeof(ProfileViewModel), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetProfile(Guid profileId)
        {
            var profile = await _profileService.GetProfileAsync(profileId);
            var profileViewModel = _mapper.Map<ProfileViewModel>(profile);

            return new JsonResult(profileViewModel);
        }

        [HttpGet("full/{profileId}")]
        [ProducesResponseType(typeof(FullProfileViewModel), (int) HttpStatusCode.OK)]
        public async Task<IActionResult> GetFullProfile(Guid profileId)
        {
            var profile = await _profileService.GetProfileAsync(profileId);
            var profileViewModel = _mapper.Map<FullProfileViewModel>(profile);

            return new JsonResult(profileViewModel);
        }
    }
}
