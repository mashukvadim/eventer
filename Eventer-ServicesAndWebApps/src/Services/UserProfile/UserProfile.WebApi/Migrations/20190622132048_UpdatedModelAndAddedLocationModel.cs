﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace UserProfile.WebApi.Migrations
{
    public partial class UpdatedModelAndAddedLocationModel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Age",
                table: "Profiles");

            migrationBuilder.RenameColumn(
                name: "Street",
                table: "Addresses",
                newName: "StreetSuffix");

            migrationBuilder.RenameColumn(
                name: "Area",
                table: "Addresses",
                newName: "StreetName");

            migrationBuilder.AddColumn<string>(
                name: "AvatarLink",
                table: "Profiles",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DateOfBirth",
                table: "Profiles",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "GenderType",
                table: "Profiles",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "LocationId",
                table: "Profiles",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Race",
                table: "Profiles",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "University",
                table: "Profiles",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Country",
                table: "Addresses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CountryCode",
                table: "Addresses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PostalCode",
                table: "Addresses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "State",
                table: "Addresses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "StateAbbr",
                table: "Addresses",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "StreetNumber",
                table: "Addresses",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.CreateTable(
                name: "Locations",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Latitude = table.Column<string>(nullable: true),
                    Longitude = table.Column<string>(nullable: true),
                    TimeZone = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Locations", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Profiles_LocationId",
                table: "Profiles",
                column: "LocationId");

            migrationBuilder.AddForeignKey(
                name: "FK_Profiles_Locations_LocationId",
                table: "Profiles",
                column: "LocationId",
                principalTable: "Locations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Profiles_Locations_LocationId",
                table: "Profiles");

            migrationBuilder.DropTable(
                name: "Locations");

            migrationBuilder.DropIndex(
                name: "IX_Profiles_LocationId",
                table: "Profiles");

            migrationBuilder.DropColumn(
                name: "AvatarLink",
                table: "Profiles");

            migrationBuilder.DropColumn(
                name: "DateOfBirth",
                table: "Profiles");

            migrationBuilder.DropColumn(
                name: "GenderType",
                table: "Profiles");

            migrationBuilder.DropColumn(
                name: "LocationId",
                table: "Profiles");

            migrationBuilder.DropColumn(
                name: "Race",
                table: "Profiles");

            migrationBuilder.DropColumn(
                name: "University",
                table: "Profiles");

            migrationBuilder.DropColumn(
                name: "Country",
                table: "Addresses");

            migrationBuilder.DropColumn(
                name: "CountryCode",
                table: "Addresses");

            migrationBuilder.DropColumn(
                name: "PostalCode",
                table: "Addresses");

            migrationBuilder.DropColumn(
                name: "State",
                table: "Addresses");

            migrationBuilder.DropColumn(
                name: "StateAbbr",
                table: "Addresses");

            migrationBuilder.DropColumn(
                name: "StreetNumber",
                table: "Addresses");

            migrationBuilder.RenameColumn(
                name: "StreetSuffix",
                table: "Addresses",
                newName: "Street");

            migrationBuilder.RenameColumn(
                name: "StreetName",
                table: "Addresses",
                newName: "Area");

            migrationBuilder.AddColumn<int>(
                name: "Age",
                table: "Profiles",
                nullable: false,
                defaultValue: 0);
        }
    }
}
