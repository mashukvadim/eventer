﻿using System;
using System.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace UserProfile.WebApi
{
    using Infrastructure;

    public static class WebHostExtensions
    {
        public static IWebHost SeedContext<TContext>(this IWebHost webHost)
            where TContext : DbContext
        {
            using (var scope = webHost.Services.CreateScope())
            {
                var services = scope.ServiceProvider;

                var context = services.GetService<TContext>();
                var env = services.GetService<IHostingEnvironment>();

                var seed = new UserProfileContextSeed();

                try
                {
                    context.Database.Migrate();
                    seed.Seed(context, env);

                }
                catch(Exception ex)
                {
                    // !!!HERE SHOULD BE LOGGER!!!
                    Debug.Write(ex.Message);
                }
            }

            return webHost;
        }
    }
}
