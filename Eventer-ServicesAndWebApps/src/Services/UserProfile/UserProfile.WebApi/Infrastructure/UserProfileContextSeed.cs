﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using UserProfile.WebApi.Models;
using UserProfile.WebApi.Models.DatabaseModels;

namespace UserProfile.WebApi.Infrastructure
{
    public class UserProfileContextSeed
    {
        internal void Seed(DbContext context, IHostingEnvironment env)
        {
            if (!(context is UserProfileContext userProfileContext))
                throw new InvalidCastException(nameof(userProfileContext));

            var contentRootPath = env.ContentRootPath;

            if (!userProfileContext.Addresses.Any())
            {
                var fullPath = Path.Combine(contentRootPath, "SeedData/Addresses.json");
                var addresses = GetItemsFromFile<Address>(fullPath);

                FillDbContext(userProfileContext, addresses);
            }

            if (!userProfileContext.Locations.Any())
            {
                var fullPath = Path.Combine(contentRootPath, "SeedData/Locations.json");
                var locations = GetItemsFromFile<Location>(fullPath);

                FillDbContext(userProfileContext, locations);
            }

            if (!userProfileContext.Profiles.Any())
            {
                var fullPath = Path.Combine(contentRootPath, "SeedData/Profiles.json");
                var profiles = GetItemsFromFile<Profile>(fullPath);

                Queue<Address> addresed = new Queue<Address>(userProfileContext.Addresses.AsEnumerable());
                Queue<Location> locations = new Queue<Location>(userProfileContext.Locations.AsEnumerable());

                foreach (var profile in profiles)
                {
                    profile.DateOfBirth = profile.DateOfBirth.Date;

                    var address = addresed.Dequeue();
                    var location = locations.Dequeue();

                    profile.Address = address;
                    profile.Location = location;
                }

                FillDbContext(userProfileContext, profiles);
            }
        }

        private IEnumerable<T> GetItemsFromFile<T>(string fullPath)
        {
            var json = File.ReadAllText(fullPath);
            var items = JsonConvert.DeserializeObject<List<T>>(json);

            return items;
        }

        private void FillDbContext<T>(DbContext context, IEnumerable<T> items)
            where T : class
        {
            context.Set<T>().AddRange(items);
            context.SaveChanges();
        }
    }
}
