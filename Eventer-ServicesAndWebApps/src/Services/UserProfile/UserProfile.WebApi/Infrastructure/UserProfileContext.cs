﻿using Microsoft.EntityFrameworkCore;
using UserProfile.WebApi.Models;
using UserProfile.WebApi.Models.DatabaseModels;

namespace UserProfile.WebApi.Infrastructure
{
    public class UserProfileContext : DbContext
    {
        public UserProfileContext(DbContextOptions<UserProfileContext> options)
            : base(options)
        {

        }

        public virtual DbSet<Profile> Profiles { get; set; }
        public virtual DbSet<Address> Addresses { get; set; }
        public virtual DbSet<Location> Locations { get; set; }
    }
}
