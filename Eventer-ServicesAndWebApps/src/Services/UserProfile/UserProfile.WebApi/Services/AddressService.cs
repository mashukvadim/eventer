﻿using System;
using System.Collections.Generic;
using System.Linq;
using UserProfile.WebApi.Infrastructure;
using UserProfile.WebApi.Models.DatabaseModels;
using UserProfile.WebApi.Services.Interfaces;

namespace UserProfile.WebApi.Services
{
    public class AddressService : IAddressService
    {
        private readonly UserProfileContext _userProfileContext;

        public AddressService(UserProfileContext userProfileContext)
        {
            _userProfileContext = userProfileContext ?? throw new ArgumentNullException(nameof(userProfileContext));
        }

        public Address GetAddress(Guid addressId)
        {
            return _userProfileContext.Addresses.FirstOrDefault(x => x.Id == addressId);
        }

        public IEnumerable<Address> GetAddresses(int skip, int take)
        {
            var address = _userProfileContext.Addresses.AsQueryable();

            if (skip > 0)
                address = address.Skip(skip);

            if (take > 0)
                address = address.Take(take);

            return address;
        }
    }
}
