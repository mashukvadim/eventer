﻿using System;
using System.Collections.Generic;
using System.Linq;
using UserProfile.WebApi.Infrastructure;
using UserProfile.WebApi.Models.DatabaseModels;
using UserProfile.WebApi.Services.Interfaces;

namespace UserProfile.WebApi.Services
{
    public class LocationService : ILocationService
    {
        private readonly UserProfileContext _userProfileContext;

        public LocationService(UserProfileContext userProfileContext)
        {
            _userProfileContext = userProfileContext ?? throw new ArgumentNullException(nameof(userProfileContext));
        }

        public Location GetLocation(Guid locationId)
        {
            return _userProfileContext.Locations.FirstOrDefault(x => x.Id == locationId);
        }

        public IEnumerable<Location> GetLocations(int skip, int take)
        {
            var location = _userProfileContext.Locations.AsQueryable();

            if (skip > 0)
                location = location.Skip(skip);

            if (take > 0)
                location = location.Take(take);

            return location;
        }
    }
}
