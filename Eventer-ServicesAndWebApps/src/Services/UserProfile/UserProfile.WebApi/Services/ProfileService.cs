﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using UserProfile.WebApi.Infrastructure;
using UserProfile.WebApi.Models.DatabaseModels;
using UserProfile.WebApi.Services.Interfaces;

namespace UserProfile.WebApi.Services
{
    public class ProfileService : IProfileService
    {
        private readonly UserProfileContext _userProfileContext;

        public ProfileService(UserProfileContext userProfileContext)
        {
            _userProfileContext = userProfileContext ?? throw new ArgumentNullException(nameof(userProfileContext));
        }

        public async Task<Profile> GetProfileAsync(Guid profileId)
        {
            return await _userProfileContext.Profiles.FirstOrDefaultAsync(x => x.Id == profileId);
        }

        public async Task<Profile> GetFullProfileAsync(Guid profileId)
        {
            return await _userProfileContext.Profiles
                .Where(x => x.Id == profileId)
                .Include(x => x.Address)
                .Include(x => x.Location)
                .FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<Profile>> GetProfilesAsync(int skip = 0, int take = 0)
        {
            var profiles = _userProfileContext.Profiles.AsQueryable();

            if (skip > 0)
                profiles = profiles.Skip(skip);

            if (take > 0)
                profiles = profiles.Take(take);

            return await profiles.ToArrayAsync();
        }
    }
}
