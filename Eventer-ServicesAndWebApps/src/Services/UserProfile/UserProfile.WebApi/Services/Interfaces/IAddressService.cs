﻿using System;
using System.Collections.Generic;
using UserProfile.WebApi.Models.DatabaseModels;

namespace UserProfile.WebApi.Services.Interfaces
{
    public interface IAddressService
    {
        Address GetAddress(Guid addressId);

        IEnumerable<Address> GetAddresses(int skip, int take);
    }
}
