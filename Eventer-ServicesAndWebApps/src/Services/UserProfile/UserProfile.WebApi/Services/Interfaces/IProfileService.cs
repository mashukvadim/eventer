﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UserProfile.WebApi.Models.DatabaseModels;

namespace UserProfile.WebApi.Services.Interfaces
{
    public interface IProfileService
    {
        Task<Profile> GetProfileAsync(Guid profileId);

        Task<Profile> GetFullProfileAsync(Guid profileId);

        Task<IEnumerable<Profile>> GetProfilesAsync(int skip = 0, int take = 0);
    }
}
