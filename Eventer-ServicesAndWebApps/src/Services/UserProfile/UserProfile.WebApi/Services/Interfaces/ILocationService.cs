﻿using System;
using System.Collections.Generic;
using UserProfile.WebApi.Models.DatabaseModels;

namespace UserProfile.WebApi.Services.Interfaces
{
    public interface ILocationService
    {
        Location GetLocation(Guid locationId);

        IEnumerable<Location> GetLocations(int skip, int take);
    }
}
