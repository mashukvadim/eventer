﻿using System;

namespace Event.WebApi.Models
{
    public class Guest
    {
        public Guid GuestId { get; set; }
        public string GuestName { get; set; }
    }
}
