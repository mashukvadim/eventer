﻿using System;
using System.Collections.Generic;
using Couchbase;
using Newtonsoft.Json;

namespace Event.WebApi.Models
{
    public class Event : Document<string>
    {
        public string Name { get; set; }

        public DateTime Time { get; set; }

        public string Place { get; set; }

        public string OwnerId { get; set; }

        public string OwnerName { get; set; }

        public List<Guest> Guests { get; set; } = new List<Guest>();

        public void SetupContent()
        {
            Content = ToString();
        }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
