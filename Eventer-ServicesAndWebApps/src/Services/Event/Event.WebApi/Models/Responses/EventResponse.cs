﻿using System.Net;

namespace Event.WebApi.Models.Responses
{
    public class EventResponse<T>
    {
        public T Items { get; set; }

        public HttpStatusCode StatusCode { get; set; }
    }
}
