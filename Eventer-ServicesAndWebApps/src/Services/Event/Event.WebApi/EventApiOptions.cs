﻿namespace Event.WebApi
{
    public class EventApiOptions
    {
        public string AzureEndpointUri { get; set; }

        public string AzurePrimaryKey { get; set; }

        public string AzureCollectionName { get; set; }

        public string AzureDatabaseName { get; set; }

        public string CouchbaseEventerBucketName { get; set; }

        public string CouchbaseEventerBucketUserName { get; set; }

        public string CouchbaseEventerBucketPassword { get; set; }

        public string CouchbaseUri { get; set; }
    }
}
