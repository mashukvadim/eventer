﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Event.WebApi.Infrastructure.Interfaces;

namespace Event.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EventController : ControllerBase
    {
        private readonly IEventRepository _eventRepository;

        public EventController(IEventRepository eventRepository)
        {
            _eventRepository = eventRepository ?? throw new ArgumentNullException(nameof(eventRepository));
        }

        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<Models.Event>), 200)]
        public async Task<IActionResult> Get(string[] eventIds)
        {
            var response = await _eventRepository.GetEventsAsync(eventIds);
            return new JsonResult(response.Items);
        }

        [HttpGet("{eventId:Guid}")]
        [ProducesResponseType(typeof(Models.Event), 200)]
        public async Task<IActionResult> Get(Guid eventId)
        {
            var response = await _eventRepository.GetEventAsync(eventId.ToString());
            return new JsonResult(response.Items);
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] Models.Event @event)
        {
            var response = await _eventRepository.CreateEventAsync(@event);
            return StatusCode((int)response.StatusCode);
        }

        [HttpPut]
        public async Task<IActionResult> Put([FromBody] Models.Event @event)
        {
            var response = await _eventRepository.UpdateEventAsync(@event);
            return StatusCode((int)response.StatusCode);
        }

        [HttpDelete("{eventId:Guid}")]
        public void Delete(Guid eventId)
        {
            throw new NotImplementedException();
        }
    }
}
