﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Net;
//using System.Threading.Tasks;
//using Microsoft.Azure.Documents;
//using Microsoft.Azure.Documents.Client;
//using Microsoft.Extensions.Options;
//using Event.WebApi.Infrastructure.Interfaces;
//using Event.WebApi.Models.Responses;

//namespace Event.WebApi.Infrastructure
//{
//    public class AzureEventRepotiory : IEventRepository
//    {
//        // UriFactory
//        private readonly string _azureCollectionName;

//        private readonly DocumentClient _client;

//        private readonly Database _database;
//        private readonly DocumentCollection _collection;

//        public AzureEventRepotiory(IOptions<EventApiOptions> options)
//        {
//            if (options == null)
//                throw new ArgumentNullException(nameof(options));

//            _azureCollectionName = options.Value.AzureCollectionName;

//            try
//            {
//                _client = new DocumentClient(new Uri(options.Value.AzureEndpointUri), options.Value.AzurePrimaryKey);
//                _database = _client.CreateDatabaseIfNotExistsAsync(new Database { Id = options.Value.AzureDatabaseName }).Result.Resource;
//                _collection = _client.CreateDocumentCollectionIfNotExistsAsync(_database.SelfLink, new DocumentCollection { Id = _azureCollectionName }).Result.Resource;
//            }
//            catch (Exception)
//            {

//                throw;
//            }
//        }

//        public EventResponse<Models.Event> GetEvent(string eventId)
//        {
//            var documentCollectionUri = UriFactory.CreateDocumentCollectionUri(_database.Id, _azureCollectionName);

//            var @event = _client.CreateDocumentQuery<Models.Event>(documentCollectionUri)
//                .Where(e => e.Id == eventId)
//                .AsEnumerable()
//                .FirstOrDefault();

//            var httpStatusCode = @event == null
//                ? HttpStatusCode.NotFound
//                : HttpStatusCode.OK;

//            return new EventResponse<Models.Event> { Items = @event, StatusCode = httpStatusCode };
//        }

//        public async Task<EventResponse<List<Models.Event>>> GetEventsAsync()
//        {
//            var documentCollectionUri = UriFactory.CreateDocumentCollectionUri(_database.Id, _azureCollectionName);

//            var events = await _client.ReadDocumentCollectionFeedAsync(documentCollectionUri);

//            return null;
//        }

//        public async Task<EventResponse<Document>> CreateEventAsync(Models.Event @event)
//        {
//            var documentCollectionUri = UriFactory.CreateDocumentCollectionUri(_database.Id, _azureCollectionName);

//            var result = await _client.CreateDocumentAsync(documentCollectionUri, @event);

//            return new EventResponse<Document> { Items = result.Resource, StatusCode = result.StatusCode };
//        }

//        public async Task<EventResponse<Document>> UpdateEventAsync(Models.Event @event)
//        {
//            var documentCollectionUri = UriFactory.CreateDocumentCollectionUri(_database.Id, _azureCollectionName);

//            var result = await _client.UpsertDocumentAsync(documentCollectionUri, @event);

//            return new EventResponse<Document> { Items = result.Resource, StatusCode = result.StatusCode };
//        }
//    }
//}
