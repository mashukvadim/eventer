﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Event.WebApi.Models.Responses;

namespace Event.WebApi.Infrastructure.Interfaces
{
    public interface IEventRepository
    {
        Task<EventResponse<Models.Event>> GetEventAsync(string eventId);

        Task<EventResponse<List<Models.Event>>> GetEventsAsync(string[] eventIds);

        Task<EventResponse<Models.Event>> CreateEventAsync(Models.Event @event);

        Task<EventResponse<Models.Event>> UpdateEventAsync(Models.Event @event);
    }
}
