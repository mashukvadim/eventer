﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Couchbase;
using Couchbase.Authentication;
using Couchbase.Configuration.Client;
using Couchbase.Core;
using Event.WebApi.Infrastructure.Interfaces;
using Event.WebApi.Models.Responses;
using Newtonsoft.Json;

namespace Event.WebApi.Infrastructure
{
    public class CouchbaseEventRepository : IEventRepository
    {
        private readonly string _couchbaseUri;

        private ICluster _cluster;
        private IBucket _eventerBucket;

        public CouchbaseEventRepository(IOptions<EventApiOptions> options)
        {
            if (options == null)
                throw new ArgumentNullException(nameof(options));

            _couchbaseUri = options.Value.CouchbaseUri;

            var couchbaseUri = new Uri(_couchbaseUri);

            var clientConfiguration = new ClientConfiguration();
            clientConfiguration.Servers = new List<Uri> { couchbaseUri };
            //clientConfiguration.BucketConfigs = new Dictionary<string, BucketConfiguration>
            //{
            //    { options.Value.CouchbaseEventerBucketName, new BucketConfiguration{BucketName = options.Value.CouchbaseEventerBucketName, Username = options.Value.CouchbaseEventerBucketUserName, Password = options.Value.CouchbaseEventerBucketPassword } }
            //};

            _cluster = new Cluster(clientConfiguration);

            var authenticator = new PasswordAuthenticator(options.Value.CouchbaseEventerBucketUserName, options.Value.CouchbaseEventerBucketPassword);
            _cluster.Authenticate(authenticator);
            _eventerBucket = _cluster.OpenBucket(options.Value.CouchbaseEventerBucketName);
        }

        public async Task<EventResponse<Models.Event>> GetEventAsync(string eventId)
        {
            var get = await _eventerBucket.GetDocumentAsync<string>(eventId);
            return new EventResponse<Models.Event>() { Items = DeserializeEvent(get.Id, get.Content) };
        }

        public async Task<EventResponse<List<Models.Event>>> GetEventsAsync(string[] eventIds)
        {
            var getDocuments = await _eventerBucket.GetDocumentsAsync<string>(eventIds);

            var result = getDocuments.Select(x => DeserializeEvent(x.Document.Id, x.Document.Content)).ToList();
            return new EventResponse<List<Models.Event>> { Items = result };
        }

        public async Task<EventResponse<Models.Event>> CreateEventAsync(Models.Event @event)
        {
            if (string.IsNullOrWhiteSpace(@event.Id))
                @event.Id = Guid.NewGuid().ToString();

            @event.SetupContent();

            var upsert = await _eventerBucket.UpsertAsync(@event);

            if (upsert.Success)
            {
                var response = await GetEventAsync(@event.Id);
                response.StatusCode = HttpStatusCode.Created;
                return response;
            }

            return new EventResponse<Models.Event>();
        }

        public async Task<EventResponse<Models.Event>> UpdateEventAsync(Models.Event @event)
        {
            var updateEvent = new Document<string> { Id = @event.Id, Content = @event.ToString() };

            var existing = await _eventerBucket.ReplaceAsync(updateEvent);

            if (existing.Status == Couchbase.IO.ResponseStatus.KeyExists)
            {
                return new EventResponse<Models.Event> { Items = null, StatusCode = HttpStatusCode.NotModified };
            }

            var resultModel = JsonConvert.DeserializeObject<Models.Event>(existing.Content);
            return new EventResponse<Models.Event> { Items = resultModel, StatusCode = HttpStatusCode.Accepted };
        }

        private static Models.Event DeserializeEvent(string id, string eventJson)
        {
            var result = JsonConvert.DeserializeObject<Models.Event>(eventJson);
            result.Id = id;

            return result;
        }
    }
}
